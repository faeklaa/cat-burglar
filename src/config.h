#pragma once

#include "ofMain.h"

namespace config{
	static const int APP_WINDOW_WIDTH = 1920;
	static const int APP_WINDOW_LIMIT = 1550;
	static const int APP_WINDOW_HEIGHT = 1080;
	static const int APP_DESIRED_FRAMERATE = 60;

	static const int GRAVITY = 9.8;
}