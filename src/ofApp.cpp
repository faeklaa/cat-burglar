#include "ofApp.h"
#include "config.h"
#include "ofMain.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowShape(config::APP_WINDOW_WIDTH, config::APP_WINDOW_HEIGHT);
	ofSetFrameRate(config::APP_DESIRED_FRAMERATE);
	ofSetVerticalSync(true);

	m_arduino.connect("COM3", 57600);
	ofAddListener(m_arduino.EInitialized, this, &ofApp::setupArduino);
	m_bSetup = false;

	for (int i = 0; i < 255; i++) {
		keyDown[i] = false;
	}

	Nami.x = 50;
	Nami.y = 600;
	Nami.v = 0;
	Nami.speed = 10;
	Nami.jump = false;
	Nami.img.load("NamiR.png");
	Nami.groundY = 600;
	Nami.jumpSpeed = 90;
	Nami.numItems = 0;

	groundY = 600;

	bg1.img.load("bg1.png");
	bg1.item[0].img.load("box1.png");
	bg1.item[0].x = 1200;
	bg1.item[0].y = 610;
	bg1.item[0].inInven = false;


	bg2.img.load("bg2.png");
	bg3.img.load("bg3.png");
	bg4.img.load("bg4.png");
	bg5.img.load("bg5.png");
	bg6.img.load("bg6.png");
	bg7.img.load("bg7.png");
	bg8.img.load("bg8.png");
	curBG = 1;
}

//--------------------------------------------------------------
void ofApp::update(){
	updateArduino();
	
	cout << Nami.numItems;

	//keybinds
	if (keyDown['1'] == true) {
		curBG = 1;
	}
	if (keyDown['2'] == true) {
		curBG = 2;
	}
	if (keyDown['3'] == true) {
		curBG = 3;
	}
	if (keyDown['4'] == true) {
		curBG = 4;
	}

	if (keyDown['5'] == true) {
		curBG = 5;
	}

	if (keyDown['6'] == true) {
		curBG = 6;
	}

	if (keyDown['7'] == true) {
		curBG = 7;
	}

	if (keyDown['8'] == true) {
		curBG = 8;
	}

	switch (curBG) {
	case 1:
		if (Collision(&Nami, &bg1.item[0])) {
			bg1.item[0].canCollect = true;
		}
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		break;
	case 6:
		break;
	case 7:
		break;
	case 8:
		break;
	}

	// movement
	if (keyDown[OF_KEY_RIGHT] && Nami.x < config::APP_WINDOW_LIMIT) {
		Nami.x += Nami.speed;
		Nami.img.load("NamiR.png");
	}

	if (keyDown[OF_KEY_LEFT] && 0 < Nami.x) {
		Nami.x -= Nami.speed;
		Nami.img.load("NamiL.png");
	}

	//NAMI JUMP CODE
	if (keyDown[OF_KEY_UP] && Nami.jump == false) {
		Nami.jump = true;
		Nami.t = 0;
		Nami.y0 = Nami.y;
		Nami.v = Nami.jumpSpeed;
	}

	if (keyDown['e']) {
		if (bg1.item[0].canCollect && !bg1.item[0].inInven) {
			Nami.addInven(&bg1.item[0]);
		}
	}

	if (Nami.jump) {
		Nami.y += Nami.v;
		Nami.v += gravity;
	}

	Nami.Jump();

	//change rooms
	if (keyDown['e'] && switchScenePopupR(&Nami)){
		switchSceneR(&Nami);
	}

	if (keyDown['e'] && switchScenePopupL(&Nami)){
		switchSceneL(&Nami);
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	//switch bg (including bg image and objects)
	switch (curBG) {
	case 1:
		bg1.img.draw(0, 0);
		if (!bg1.item[0].inInven) {
			bg1.item[0].img.draw(bg1.item[0].x, bg1.item[0].y);
		}
		break;
	case 2:
		bg2.img.draw(0, 0);
		break;
	case 3:
		bg3.img.draw(0, 0);
		break;
	case 4:
		bg4.img.draw(0, 0);
		break;
	case 5:
		bg5.img.draw(0, 0);
		break;
	case 6:
		bg6.img.draw(0, 0);
		break;
	case 7:
		bg7.img.draw(0, 0);
		break;
	case 8:
		bg8.img.draw(0, 0);
		break;
	}

	Nami.img.draw(Nami.x, Nami.y);
	if (switchScenePopupR(&Nami)) {
		ofDrawRectangle(Nami.x, Nami.y, 100, 100);
	}

	if (switchScenePopupL(&Nami)) {
		ofDrawRectangle(Nami.x, Nami.y, 100, 100);
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	keyDown[key] = true;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	keyDown[key] = false;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

bool ofApp::switchScenePopupR(player* Nami) {
	if (Nami->x < config::APP_WINDOW_WIDTH && Nami->x > 1500 && curBG < 8) {
		return true;
	}
	return false;
}

bool ofApp::switchScenePopupL(player* Nami) {
	if (Nami->x < 50 && Nami->x > -10 && curBG > 1) {
		return true;
	}
	return false;
}

void ofApp::switchSceneR(player* Nami) {
	Nami->x = 51;
	curBG++;
}

void ofApp::switchSceneL(player* Nami) {
	Nami->x = 1499;
	curBG--;
}

bool ofApp::Collision(player* Nami, Item* item) {
	if (item->x < Nami->x + Nami->img.getWidth() / 2 && item->x > Nami->x - Nami->img.getWidth() / 2) {
		if (Nami->y < item ->y && Nami->y > (item->y - item->img.getHeight())) {
			return true;
		}
	}
	return false;
}

//--------------------------------------------------------------
void ofApp::setupArduino(const int& version) {

	// remove listener because we don't need it anymore
	ofRemoveListener(m_arduino.EInitialized, this, &ofApp::setupArduino);

	// it is now safe to send commands to the Arduino
	m_bSetup = true;


	// set pins D2 and A5 to digital input
	m_arduino.sendDigitalPinMode(2, ARD_INPUT);
	m_arduino.sendDigitalPinMode(3, ARD_INPUT);
	m_arduino.sendDigitalPinMode(4, ARD_INPUT);  
	m_arduino.sendDigitalPinMode(5, ARD_INPUT);

	m_arduino.sendAnalogPinReporting(0, ARD_ANALOG);

	ofAddListener(m_arduino.EDigitalPinChanged, this, &ofApp::digitalPinChanged);
	ofAddListener(m_arduino.EAnalogPinChanged, this, &ofApp::analogPinChanged);
}
//--------------------------------------------------------------
void ofApp::updateArduino() {

	m_arduino.update();

	// do not send anything until the arduino has been set up
	//if (m_bSetup) {
		// fade the led connected to pin D11
		//m_arduino.sendPwm(11, (int)(128 + 128 * sin(ofGetElapsedTimef())));   // pwm...
	//}

}


//--------------------------------------------------------------
void ofApp::digitalPinChanged(const int& pinNum) {
	// do something with the digital input. here we're simply going to print the pin number and
	// value to the screen each time it changes
	switch (pinNum) {
	case 2:
		cout << "2" << endl;
		break;
	case 3:
		cout << "3" << endl;
		break;
	case 4:
		cout << "4" << endl;
		break;
	case 5:
		cout << "5" << endl;
		break;
	}

}

//--------------------------------------------------------------
void ofApp::analogPinChanged(const int& pinNum) {
	// do something with the analog input. here we're simply going to print the pin number and
	// value to the screen each time it changes
	if (m_arduino.getAnalog(0) > 1000) {
		std::cout << " ANALOG SLAY" << endl;
	}
}