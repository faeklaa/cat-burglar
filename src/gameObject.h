#include "ofMain.h"
#pragma once

class gameObject
{
	public:
		int x;
		float y;
		ofImage img;

		gameObject();
};

class Item : public gameObject {
public:
	bool inInven;
	bool canCollect;
	bool isPerma;
};

class Scene : public gameObject {
public:
	Item item[5];
};

class player : public gameObject {
	public:
		int speed;
		int curRoom;

		int jumpSpeed;
		int groundY;
		bool jump;
		int y0;
		int v;
		float t;

		Item* inven[16];
		int numItems;

		void Jump();
		void addInven(Item* item);
};
