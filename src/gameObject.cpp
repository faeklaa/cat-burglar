#include "gameObject.h"
#include "config.h"

// gameobject functions
gameObject::gameObject() {
	x = 0;
	y = 0;
}

void player::Jump() {
	//std::cout << curY << endl;
	//move
	if (jump) {
		y = y0 - v * t + 0.5 * config::GRAVITY * t * t;
		t++;
		if (y > groundY) {
			y = groundY;
			jump = false;
		}
	}
	else //see if we have walked off a cliff and have to fall
	{
		if (y < groundY) {
			jump = true;
			t = 0;
			y0 = y;
			v = 0;
		}
	}
}

void player::addInven(Item *item) {
	item->inInven = true;
	inven[numItems] = item;
	numItems++;
}
