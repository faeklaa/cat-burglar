#pragma once

#include "ofMain.h"
#include "gameObject.h"
#include "ofEvents.h"

class ofApp : public ofBaseApp{

	public:
		//open frameworks base functions
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mousePressed(int x, int y, int button);

		bool keyDown[255];

		player Nami;
		float gravity;
		float groundY;
		
		// backgounds n stuff
		int curBG;
		Scene bg1;
		Scene bg2;
		Scene bg3;
		Scene bg4;
		Scene bg5;
		Scene bg6;
		Scene bg7;
		Scene bg8;

		bool switchScenePopupR(player* Nami);
		void switchSceneR(player* Nami);
		bool switchScenePopupL(player* Nami);
		void switchSceneL(player* Nami);

		bool Collision(player* Nami, Item* item);

		ofArduino	m_arduino;
		bool		m_bSetup;			// flag variable for setting up arduino once

private:

	void setupArduino(const int& version);
	void digitalPinChanged(const int& pinNum);
	void analogPinChanged(const int& pinNum);
	void updateArduino();
};

